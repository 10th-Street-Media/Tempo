# Tempo

Tempo is software aimed at artists of all types who want to build their online profiles and interact with fans of their work. It includes blogging and microblogging features, as well as media tools to display their work, and a calendar to help people book them for concerts, readings, residencies, or gallery shows.
